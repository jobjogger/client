export default class Environment {
  static get CONFIG () {
    return {
      oAuthClientId: '$VUE_APP_OAUTH_CLIENT_ID',
      oAuthClientSecret: '$VUE_APP_OAUTH_CLIENT_SECRET',
      clientHost: '$VUE_APP_CLIENT_HOST',
      authServerHost: '$VUE_APP_AUTH_SERVER_HOST',
      resourceServerHost: '$VUE_APP_RESOURCE_SERVER_HOST',
      reportingServiceHost: '$VUE_APP_REPORTING_SERVICE_HOST',
      dashboardHost: '$VUE_APP_DASHBOARD_HOST',
    }
  }

  static value (name) {
    if (!(name in this.CONFIG)) return ''

    const value = this.CONFIG[name]

    if (!value) return ''

    if (!value.startsWith('$VUE_APP_')) return value

    const envName = value.substr(1), envValue = process.env[envName]

    if (!envValue) return ''

    return envValue
  }
}
