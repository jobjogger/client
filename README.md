# JobJogger Client
This repository contains the code of our JobJogger Client. It's the user-interface to use the complete platform.
In addition, the Client does all the End-to-End tests and API endpoint tests.

## Local Development with docker-compose
When developing locally it's generally easier (and faster) to let the Client run in docker-compose, instead of the
minikube cluster. This allows you to develop with **Hot-Reloading** that's provided by Webpack.

The container can run at the same time like the cluster deployment. Start the container by executing:
```bash
docker-compose up
```
To route the **jobjogger.local** domain to the docker container, change your `/etc/hosts` file to contain:
```
127.0.0.1 jobjogger.local
```

## Tests
To execute the unit tests you can execute:
```bash
yarn test:unit
```

To execute the E2E tests locally you can execute:
```bash
docker-compose -f docker-compose.ci.local.yml up --build
```

## Webpack Aliases
We use Webpack aliases in this project (import modules with `@` as base). 
This always works at build-time but IntelliJ doesn't recognize the paths by default.
Change the following setting to help IntelliJ:
1. Open IntelliJ settings
2. Open Languages & Frameworks > JavaScript > Webpack
3. Set `node_modules/@vue/cli-service/webpack.config.js` in the dialog
