#!/usr/bin/env sh
set -eu

# Delete default config
ls -al /etc/nginx/conf.d
rm -f /etc/nginx/conf.d/default.conf
ls -al /etc/nginx/conf.d

# Replace env vars in nginx.conf
envsubst '${VUE_APP_AUTH_SERVER_HOST}' < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/nginx.conf

# Replace env vars in JavaScript files
echo "Replace env vars in JavaScript files"
for file in /usr/share/nginx/html/js/app.*.js /usr/share/nginx/html/static/auth/job-jogger*.js; do
  echo "Replace env vars in JavaScript file $file"

  # Use the existing JS file as template
  if [ ! -f $file.template.js ]; then
    cp $file $file.template.js
  fi

  envsubst '$VUE_APP_OAUTH_CLIENT_ID,$VUE_APP_OAUTH_CLIENT_SECRET,$VUE_APP_CLIENT_HOST,$VUE_APP_AUTH_SERVER_HOST,$VUE_APP_RESOURCE_SERVER_HOST,$VUE_APP_REPORTING_SERVICE_HOST,$VUE_APP_DASHBOARD_HOST' < $file.template.js > $file
done

exec "$@"
