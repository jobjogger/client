const MonacoEditorPlugin = require('monaco-editor-webpack-plugin')

module.exports = {
  configureWebpack: {
    output: {
      filename: 'js/[name].[hash].js',
    },
    plugins: [
      new MonacoEditorPlugin({
        // https://github.com/Microsoft/monaco-editor-webpack-plugin#options
        languages: ['dockerfile', 'go', 'html', 'java', 'javascript', 'json',
          'markdown', 'php', 'python', 'r', 'shell', 'typescript', 'yaml'],
      }),
    ],
  },
  devServer: {
    historyApiFallback: true, // TODO: Needed?
    headers: {
      // TODO: Only set CORS headers on needed routes
      'Access-Control-Allow-Origin': process.env.VUE_APP_AUTH_SERVER_HOST,
      'Access-Control-Allow-Methods': 'GET, OPTIONS',
      'Access-Control-Allow-Headers': 'Authorization, X-XSRF-TOKEN',
    },
    host: '0.0.0.0',
    hot: true,
    disableHostCheck: true,
  },
}
