FROM nginx:stable-alpine
COPY dist /usr/share/nginx/html
COPY docker/nginx/nginx.conf.template /etc/nginx/conf.d
COPY docker/entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
