import { ImageSortingTypes, JobStatusTypes, JobStatusFilteringTypes, JobSortingTypes } from '@/globals'

export default {
  computed: {
    $imageSortingTypes () {
      return ImageSortingTypes
    },
    $jobStatusTypes () {
      return JobStatusTypes
    },
    $jobStatusFilteringTypes () {
      return JobStatusFilteringTypes
    },
    $jobSortingTypes () {
      return JobSortingTypes
    },
  },
  methods: {
    $typeByJobStatus (id) {
      const status = JobStatusTypes[id]
      if (status === undefined) return 'info'

      return status.tagType
    },
  },
}
