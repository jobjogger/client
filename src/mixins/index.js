import Vue from 'vue'

import app from '@/mixins/app'
import auth from '@/mixins/auth'
import inbox from '@/mixins/inbox'
import user from '@/mixins/user'
import jobs from '@/mixins/jobs'

Vue.mixin(app)
Vue.mixin(auth)
Vue.mixin(inbox)
Vue.mixin(user)
Vue.mixin(jobs)
