export default {
  methods: {
    $typeByMessageType (status) {
      switch (status) {
        case 'JOB_SUCCEEDED':
          return 'success'
        case 'JOB_TIMED_OUT':
        case 'JOB_TAKING_LONG':
        case 'JOB_CANCELED':
          return 'warning'
        case 'JOB_FAILED':
          return 'danger'
        default:
          return 'info'
      }
    },
    $labelByMessageType (status) {
      switch (status) {
        case 'JOB_SUCCEEDED':
          return 'Job succeeded'
        case 'JOB_TIMED_OUT':
          return 'Job timed out'
        case 'JOB_TAKING_LONG':
          return 'Job is taking long'
        case 'JOB_CANCELED':
          return 'Job was canceled'
        case 'JOB_FAILED':
          return 'Job failed'
        default:
          return 'Job status changed'
      }
    },
  },
}
