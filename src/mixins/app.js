export default {
  computed: {
    $iconUrl () {
      return `${process.env.BASE_URL}static/images/jobjogger-icon.png`
    },
    $logoUrl () {
      return `${process.env.BASE_URL}static/images/jobjogger-logo.png`
    },
    $logoDarkUrl () {
      return `${process.env.BASE_URL}static/images/jobjogger-logo-dark.png`
    },
    $isLoading () {
      return this.$store.getters['app/isLoading']
    },
  },
}
