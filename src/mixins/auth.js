export default {
  computed: {
    $isLoginRoute () {
      return this.$route.path === '/auth/login'
    },
    $isSignUpRoute () {
      return this.$route.path === '/auth/sign-up'
    },
  },
}
