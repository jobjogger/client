export default {
  computed: {
    $currentUser () {
      return this.$store.state.user.currentUser
    },
    $userIsLoggedIn () {
      return this.$store.getters['user/loggedIn']
    },
    $userIsAdmin () {
      return this.$store.getters['user/isAdmin']
    },
  },
}
