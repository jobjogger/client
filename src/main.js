import Vue from 'vue'
import router from '@/router/index'
import store from '@/store/index'

import './mixins'

import ElementUI from 'element-ui'

import App from '@/App'

Vue.config.productionTip = false

Vue.use(ElementUI)

export default new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
