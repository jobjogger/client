export const ImageSortingTypes = {
  UPLOAD: {
    id: 'upload',
    name: 'Uploaded Time',
    property: 'uploadTime',
    transform: date => new Date(date),
    direction: 'DESC',
  },
  NAME: {
    id: 'name',
    name: 'Name',
    property: 'name',
    transform: null,
    direction: 'ASC',
  },
  PROJECT: {
    id: 'project',
    name: 'Project',
    property: 'project',
    transform: null,
    direction: 'ASC',
  },
}

export const JobStatusTypes = {
  NONE: {
    id: 'NONE',
    tagType: 'info',
    watched: false,
    order: 7,
  },
  STARTING: {
    id: 'STARTING',
    tagType: 'success',
    watched: true,
    order: 2,
  },
  PENDING: {
    id: 'PENDING',
    tagType: 'info',
    watched: true,
    order: 1,
  },
  RUNNING: {
    id: 'RUNNING',
    tagType: 'success',
    watched: true,
    order: 0,
  },
  CANCELLING: {
    id: 'CANCELLING',
    tagType: 'warning',
    watched: true,
    order: 3,
  },
  CANCELED: {
    id: 'CANCELED',
    tagType: 'warning',
    watched: false,
    order: 6,
  },
  SUCCEEDED: {
    id: 'SUCCEEDED',
    tagType: 'success',
    watched: false,
    order: 5,
  },
  FAILED: {
    id: 'FAILED',
    tagType: 'danger',
    watched: false,
    order: 4,
  },
}

export const JobStatusFilteringTypes = {
  ALL: {
    id: 'ALL',
    name: 'All Jobs',
    endpoint: '/jobs',
  },
  PENDING: {
    id: 'PENDING',
    name: 'Pending Jobs',
    endpoint: '/jobs/status/pending',
  },
  RUNNING: {
    id: 'RUNNING',
    name: 'Running Jobs',
    endpoint: '/jobs/status/running',
  },
  SUCCEEDED: {
    id: 'SUCCEEDED',
    name: 'Succeeded Jobs',
    endpoint: '/jobs/status/succeeded',
  },
  FAILED: {
    id: 'FAILED',
    name: 'Failed Jobs',
    endpoint: '/jobs/status/failed',
  },
  CANCELED: {
    id: 'CANCELED',
    name: 'Canceled Jobs',
    endpoint: '/jobs/status/canceled',
  },
}

const JobStatusSortingOrder = Object.values(JobStatusTypes).reduce((types, type) => {
  types[type.id] = type.order

  return types
}, {})

export const JobSortingTypes = {
  CREATED: {
    id: 'created',
    name: 'Created Time',
    property: 'createTime',
    transform: date => new Date(date),
    direction: 'DESC',
  },
  NAME: {
    id: 'name',
    name: 'Name',
    property: 'name',
    transform: null,
    direction: 'ASC',
  },
  STATUS: {
    id: 'status',
    name: 'Status',
    property: 'status',
    transform: status => JobStatusSortingOrder[status],
    direction: 'ASC',
  },
}
