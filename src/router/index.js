import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from '@/router/routes'

import AppMiddleware from '@/router/middlewares/AppMiddleware'
import AuthMiddleware from '@/router/middlewares/AuthMiddleware'
import CurrentUserStateMiddleware from '@/router/middlewares/CurrentUserStateMiddleware'
import PageTitleMiddleware from '@/router/middlewares/PageTitleMiddleware'
import WatchersMiddleware from '@/router/middlewares/WatchersMiddleware'

Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  mode: 'history',
  base: process.env.BASE_URL,
})

router.beforeEach(CurrentUserStateMiddleware)
router.beforeEach(PageTitleMiddleware)
router.beforeEach(WatchersMiddleware)
router.beforeResolve(AuthMiddleware)
router.afterEach(AppMiddleware)

export default router
