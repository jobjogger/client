import $store from '@/store'
import * as authService from '@/services/auth.service'

/**
 * This middleware hooks in and checks if the user requested a route that requires auth.
 *
 * If no auth is required, the router just proceeds. If auth is required and the user is not logged in, the user will be
 * redirected to the login screen.
 *
 * If the route requires explicitly no auth, like login and sign-up, the user will be redirected to the dashboard.
 *
 * @param {object} to - The route where the user goes to
 * @param {object} from - The route where the user comes from
 * @param {function} next - A callback function that will proceed the routing
 *
 */
export default function AuthMiddleware (to, from, next) {
  const userIsLoggedIn = $store.getters['user/loggedIn']

  if (userIsLoggedIn && to.matched.some(route => route.meta.requiresNoAuth))
    return next({ name: 'dashboard' })

  if (userIsLoggedIn || !to.matched.some(route => route.meta.requiresAuth))
    return next()

  authService.authorize()
}
