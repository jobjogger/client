import $store from '@/store'
import { Message } from 'element-ui'
import * as authService from '@/services/auth.service'

/**
 * This middleware hooks in and checks the current user state.
 *
 * @param {object} to - The route where the user goes to
 * @param {object} from - The route where the user comes from
 * @param {function} next - A callback function that will proceed the routing
 *
 * @WARN Must be always first in middleware chain.
 *
 */
export default function CurrentUserStateMiddleware (to, from, next) {
  if (!$store.getters['user/loggedIn'] && authService.getAccessToken()) {
    $store
      .dispatch('user/getCurrent')
      .then(() => next())
      .catch(error => {
        if (!error) return
        Message.info({
          message: 'Your access token is not valid. Please login again.',
          offset: 36,
        })
        authService.removeAccessToken()
      })
  }

  next()
}
