import $store from '@/store'

/**
 * This middleware hooks in and resets all the App watchers.
 *
 * @param {object} to - The route where the user goes to
 * @param {object} from - The route where the user comes from
 * @param {function} next - A callback function that will proceed the routing
 *
 */
export default function WatchersMiddleware (to, from, next) {
  $store
    .dispatch('app/resetWatchers')
    .then(() => next())
}
