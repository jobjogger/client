import $store from '@/store'

/**
 * This middleware hooks in when the app is loaded and disables the
 * loading spinner.
 */
export default function AppMiddleware () {
  $store
    .dispatch('app/finishLoading')
    .then(() => {})
    .catch(() => {})
}
