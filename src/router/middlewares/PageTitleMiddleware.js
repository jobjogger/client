/**
 * This middleware hooks in and sets the page title.
 *
 * @param {object} to - The route where the user goes to
 * @param {object} from - The route where the user comes from
 * @param {function} next - A callback function that will proceed the routing
 *
 */
export default function PageTitleMiddleware (to, from, next) {
  const pageTitle = to.matched.find(item => item.meta.title)

  if (pageTitle) window.document.title = pageTitle.meta.title + ' | JobJogger'

  next()
}
