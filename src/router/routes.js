import AppConfig from '@/config'
import { Message } from 'element-ui'
import * as authService from '@/services/auth.service'

const Index = () => import('../views/Index.vue')
const Dashboard = () => import('../views/Dashboard.vue')
const ImageDetails = () => import('../views/images/ImageDetails.vue')
const ImagesOverview = () => import('../views/images/ImagesOverview.vue')
const UploadDockerimage = () => import('../views/images/UploadDockerimage.vue')
const BuildDockerimageTemplateSelection = () => import('../views/images/BuildDockerimageTemplateSelection.vue')
const BuildDockerimage = () => import('../views/images/BuildDockerimage.vue')
const EditDockerimageSource = () => import( '../views/images/EditDockerimageSource')
const JobDetails = () => import('../views/jobs/JobDetails.vue')
const JobsOverview = () => import('../views/jobs/JobsOverview.vue')
const Inbox = () => import('../views/Inbox.vue')

export default [
  {
    path: '/',
    name: 'index',
    component: Index,
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      requiresAuth: true,
      title: 'Dashboard',
    },
  },
  {
    path: '/admin/dashboard',
    name: 'admin-dashboard',
    beforeEnter () {
      const token = authService.getAccessToken()
      window.open(`${AppConfig.hosts.dashboard}?token=${token}`, '_blank')
    },
  },
  {
    path: '/images/overview',
    name: 'images-overview',
    component: ImagesOverview,
    meta: {
      requiresAuth: true,
      title: 'Images Overview',
    },
  },
  {
    path: '/images/details/:id',
    name: 'images-details',
    components: {
      default: ImagesOverview,
      side: ImageDetails,
    },
    meta: {
      requiresAuth: true,
      title: 'Image Details',
    },
  },
  {
    path: '/images/upload',
    name: 'images-upload-dockerimage',
    component: UploadDockerimage,
    meta: {
      requiresAuth: true,
      title: 'Upload Dockerimage',
    },
  },
  {
    path: '/images/edit/:id',
    name: 'images-edit-dockerimage',
    component: EditDockerimageSource,
    meta: {
      requiresAuth: true,
      title: 'Edit Dockerimage',
    },
  },
  {
    path: '/images/build',
    name: 'images-build-dockerimage',
    component: BuildDockerimageTemplateSelection,
    meta: {
      requiresAuth: true,
      title: 'Build Dockerimage',
    },
  },
  {
    path: '/images/build/template/:key',
    name: 'images-build-dockerimage-template',
    component: BuildDockerimage,
    meta: {
      requiresAuth: true,
      title: 'Build Dockerimage',
    },
  },
  {
    path: '/jobs/overview',
    name: 'jobs-overview',
    component: JobsOverview,
    meta: {
      requiresAuth: true,
      title: 'Jobs Overview',
    },
  },
  {
    path: '/jobs/details/:id',
    name: 'jobs-details',
    components: {
      default: JobsOverview,
      side: JobDetails,
    },
    meta: {
      requiresAuth: true,
      title: 'Job Details',
    },
  },
  {
    path: '/auth/callback',
    name: 'auth-callback',
    beforeEnter (to, from, next) {
      authService
        .callback(to.fullPath)
        .then(() => next({ name: 'dashboard' }))
        .catch(error => {
          Message.info({
            message: `There was an error during the authentication process (${error}). Please login again.`,
            offset: 36,
          })
          setTimeout(() => authService.authorize(), 3500)
        })
    },
  },
  {
    path: '/auth/logout',
    name: 'auth-logout',
    beforeEnter (to, from, next) {
      authService
        .logout()
        .finally(() => next({ name: 'index' }))
    },
  },
  {
    path: '/inbox',
    name: 'inbox',
    component: Inbox,
    meta: {
      requiresAuth: true,
      title: 'Inbox',
    },
  },
  {
    path: '/404',
    name: '404',
    component: () => import('../views/errors/404.vue'),
    meta: {
      title: 'Error: Not Found',
    },
  },
  {
    path: '*',
    redirect: '404',
  },
]
