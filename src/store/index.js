import 'es6-promise/auto'

import Vue from 'vue'
import Vuex from 'vuex'

import api from '@/store/modules/api'
import app from '@/store/modules/app'
import user from '@/store/modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    api,
    app,
    user,
  },
})

export default store
