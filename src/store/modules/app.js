import $store from '@/store'
import { ImageSortingTypes, JobStatusTypes, JobStatusFilteringTypes, JobSortingTypes } from '@/globals'

const watchedJobStatusTypes = Object.values(JobStatusTypes)
  .filter(status => status.watched === true)
  .map(status => status.id)

const state = {
  isLoading: true,
  watchers: {
    jobs: [],
  },
  overviews: {
    images: {
      docker: {
        sorting: ImageSortingTypes.UPLOAD,
      },
    },
    jobs: {
      filtering: JobStatusFilteringTypes.ALL,
      sorting: JobSortingTypes.CREATED,
    },
  },
}

const getters = {
  isLoading: state => {
    return state.isLoading
  },
  watchedJobs: state => {
    const jobs = state.watchers.jobs
      .map(jobId => $store.getters['api/jobWithId'](jobId))
      .filter(job => job !== null)

    return jobs
      .filter(job => watchedJobStatusTypes.includes(job.status))
      .map(job => job.id)
  },
  selectedDockerimagesOverviewSorting: state => {
    return state.overviews.images.docker.sorting
  },
  selectedJobsOverviewFiltering: state => {
    return state.overviews.jobs.filtering
  },
  selectedJobsOverviewSorting: state => {
    return state.overviews.jobs.sorting
  },
}

const actions = {
  startLoading ({ commit }) {
    commit('SET_LOADING', true)
  },
  finishLoading ({ commit }) {
    setTimeout(() => commit('SET_LOADING', false), 500)
  },
  watchJobs ({ commit }, ids) {
    commit('SET_WATCHERS_JOBS', ids.filter(id => !!id))
  },
  resetWatchers ({ commit }) {
    commit('SET_WATCHERS_JOBS', [])
  },
  selectDockerimagesOverviewSorting ({ commit }, sorting) {
    commit('SET_IMAGES_DOCKER_OVERVIEW_SORTING', sorting)
  },
  selectJobsOverviewFiltering ({ commit }, filtering) {
    commit('SET_JOBS_OVERVIEW_FILTERING', filtering)
  },
  selectJobsOverviewSorting ({ commit }, sorting) {
    commit('SET_JOBS_OVERVIEW_SORTING', sorting)
  },
}

const mutations = {
  SET_LOADING (state, isLoading) {
    state.isLoading = isLoading
  },
  SET_WATCHERS_JOBS (state, jobs) {
    state.watchers.jobs = jobs
  },
  SET_IMAGES_DOCKER_OVERVIEW_SORTING (state, sorting) {
    state.overviews.images.docker.sorting = sorting
  },
  SET_JOBS_OVERVIEW_FILTERING (state, filtering) {
    state.overviews.jobs.filtering = filtering
  },
  SET_JOBS_OVERVIEW_SORTING (state, sorting) {
    state.overviews.jobs.sorting = sorting
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
