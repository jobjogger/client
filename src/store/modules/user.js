import * as userService from '@/services/user.service'

const state = {
  currentUser: {
    active: false,
    expires: 0,
    username: '',
    authorities: [],
  },
}

const getters = {
  loggedIn: state => {
    return !!state.currentUser.active &&
      !!state.currentUser.username
  },
  isAdmin: state => {
    return getters.loggedIn &&
      state.currentUser &&
      state.currentUser.authorities &&
      state.currentUser.authorities.includes('ROLE_ADMIN')
  },
  username: state => {
    return state.currentUser.username
  },
}

const actions = {
  getCurrent ({ commit }) {
    return new Promise((resolve, reject) => {
      userService
        .getCurrent()
        .then(user => {
          commit('SET_CURRENT_USER', user)
          resolve()
        })
        .catch(error => reject(error))
    })
  },
  removeCurrent ({ commit }) {
    commit('SET_CURRENT_USER', {})
  },
}

const mutations = {
  SET_CURRENT_USER (state, currentUserData) {
    state.currentUser = {
      active: currentUserData.active,
      expires: currentUserData.exp,
      username: currentUserData.user_name,
      authorities: currentUserData.authorities,
    }
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
