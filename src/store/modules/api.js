import Vue from 'vue'
import $store from '@/store'

const state = {
  images: {
    docker: null,
  },
  jobs: null,
}

const getters = {
  allDockerimages: state => {
    if (state.images.docker === null) return null

    return Object.keys(state.images.docker).map(id => state.images.docker[id])
  },
  dockerimageWithId: state => id => {
    if (state.images.docker === null) return null

    const image = state.images.docker[id]

    if (!image || image.id !== id) return null

    return image
  },
  allJobs: state => {
    if (state.jobs === null) return null

    return Object.keys(state.jobs).map(id => state.jobs[id])
  },
  jobWithId: state => id => {
    if (state.jobs === null) return null

    const job = state.jobs[id]

    if (!job || job.id !== id) return null

    return job
  },
}

const actions = {
  resetDockerimages ({ commit }, data) {
    commit('RESET_IMAGES_DOCKER', data)
  },
  readAllDockerimages ({ commit }, data) {
    commit('PREPARE_IMAGES_DOCKER')
    data.forEach(image => commit('SET_ONE_IMAGE_DOCKER', image))
    data.flatMap(image => image.jobs).forEach(job => commit('SET_ONE_JOB', job))
  },
  readOneDockerimage ({ commit }, data) {
    commit('SET_ONE_IMAGE_DOCKER', data)
    if (data.jobs) data.jobs.forEach(job => commit('SET_ONE_JOB', job))
  },
  updateDockerimage ({ commit }, data) {
    commit('UPDATE_IMAGE_DOCKER', data)
    if (data.jobs) data.jobs.forEach(job => commit('UPDATE_JOB', job))
  },
  deleteDockerimage ({ commit }, id) {
    commit('DELETE_IMAGE_DOCKER', id)
  },
  resetJobs ({ commit }, data) {
    commit('RESET_JOBS', data)
  },
  readAllJobs ({ commit }, data) {
    commit('PREPARE_JOBS')
    data.forEach(job => commit('SET_ONE_JOB', job))
  },
  readOneJob ({ commit }, data) {
    commit('SET_ONE_JOB', data)
  },
  updateJob ({ commit }, data) {
    commit('UPDATE_JOB', data)
  },
  deleteJob ({ commit }, id) {
    commit('DELETE_JOB', id)
  },
}

const mutations = {
  RESET_IMAGES_DOCKER (state, data) {
    const image = $store.getters['api/dockerimageWithId'](data.exclude)

    if (image === null) {
      state.images.docker = null
    } else {
      const images = {}
      images[image.id] = image
      state.images.docker = images
    }
  },
  PREPARE_IMAGES_DOCKER (state) {
    if (state.images.docker !== null) return

    state.images.docker = {}
  },
  SET_ONE_IMAGE_DOCKER (state, data) {
    mutations.PREPARE_IMAGES_DOCKER(state)

    if (!data || !data.id) return

    Vue.set(state.images.docker, data.id, data)
  },
  UPDATE_IMAGE_DOCKER (state, data) {
    if (!data || !data.id) return

    const image = $store.getters['api/dockerimageWithId'](data.id)

    if (image === null) return

    mutations.SET_ONE_IMAGE_DOCKER(state, { ...image, ...data })
  },
  DELETE_IMAGE_DOCKER (state, id) {
    if (!id) return

    mutations.PREPARE_IMAGES_DOCKER(state)

    const image = $store.getters['api/dockerimageWithId'](id)

    if (image === null) return

    Vue.delete(state.images.docker, image.id)

    image.jobs.forEach(job => mutations.DELETE_JOB(state, job.id))
  },
  RESET_JOBS (state, data) {
    const job = $store.getters['api/jobWithId'](data.exclude)

    if (job === null) {
      state.jobs = null
    } else {
      const jobs = {}
      jobs[job.id] = job
      state.jobs = jobs
    }
  },
  PREPARE_JOBS (state) {
    if (state.jobs !== null) return

    state.jobs = {}
  },
  SET_ONE_JOB (state, data) {
    mutations.PREPARE_JOBS(state)

    if (!data || !data.id) return

    Vue.set(state.jobs, data.id, data)
  },
  UPDATE_JOB (state, data) {
    if (!data || !data.id) return

    const job = $store.getters['api/jobWithId'](data.id)

    if (job === null) return

    mutations.SET_ONE_JOB(state, { ...job, ...data })
  },
  DELETE_JOB (state, id) {
    if (!id) return

    mutations.PREPARE_JOBS(state)

    Vue.delete(state.jobs, id)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
