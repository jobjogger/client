import * as apiService from '@/services/api.service'

/**
 * Gets the current user data from the auth server.
 *
 * @return {Promise}
 */
export function getCurrent () {
  return new Promise((resolve, reject) => {
    apiService
      .checkToken()
      .then(user => resolve(user))
      .catch(error => reject(error))
  })
}
