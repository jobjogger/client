import axios from 'axios'
import * as authService from '@/services/auth.service'
import $store from '@/store'
import AppConfig from '@/config'
import { JobStatusFilteringTypes } from '@/globals'

/**
 * Checks the current access token at the auth server and gets the user details.
 *
 * @return {Promise}
 */
export function checkToken () {
  return new Promise((resolve, reject) => {
    const instance = axios.create({
      baseURL: AppConfig.hosts.authServer,
    })

    const accessToken = authService.getAccessToken()

    if (!accessToken) reject()

    instance
      .post(AppConfig.endpoints.authServer.checkToken, null, {
        params: { token: accessToken },
      })
      .then(response => resolve(response.data))
      .catch(error => reject(error))
  })
}

/**
 * Revokes the current access token at the auth server.
 *
 * @return {Promise}
 */
export function revokeToken () {
  return new Promise((resolve, reject) => {
    createAuthRequest()
      .delete(AppConfig.endpoints.authServer.revokeToken)
      .then(() => resolve())
      .catch(() => reject())
  })
}

/**
 * Creates a request instance for the auth server
 * that puts the access token into the header.
 */
export function createAuthRequest() {
  return createApiRequest({ baseURL: AppConfig.hosts.authServer })
}

/**
 * Creates a request instance for the resource server
 * that puts the access token into the header.
 */
export function createResourceRequest() {
  return createApiRequest({ baseURL: AppConfig.hosts.resourceServer })
}

/**
 * Creates a request instance for the reporting service server
 * that puts the access token into the header.
 *
 * @return {AxiosInstance}
 */
export function createReportingRequest() {
  return createApiRequest({ baseURL: AppConfig.hosts.reportingService })
}

/**
 * Creates a general request instance for the API
 * that puts the access token into the header.
 *
 * @param {{baseURL: string}} options - The request options
 * @return {AxiosInstance}
 */
export function createApiRequest(options) {
  const instance = axios.create(options)

  instance.interceptors.request.use(
    request => {
      const accessToken = authService.getAccessToken()

      if (!accessToken) return request

      request.headers['Authorization'] = `Bearer ${accessToken}`

      return request
    },
    error => Promise.reject(error),
  )

  instance.interceptors.response.use(
    response => response,
    error => Promise.reject(error.response.data || {}),
  )

  return instance
}

/**
 * Reads all inbox messages from the API.
 *
 * @return {Promise}
 */
export function readInboxMessages () {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .get(`/inbox/${username}`, {})
      .then(response => resolve(response.data))
      .catch(() => reject('Something went wrong while reading all inbox messages.'))
  })
}

/**
 * Reads all inbox messages from the API.
 *
 * @param {array} ids - The Message ids
 * @return {Promise}
 */
export function markInboxMessagesAsSeen (ids) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .patch(`/inbox/${username}`, { ids })
      .then(response => resolve(response.data))
      .catch(() => reject('Something went wrong while marking inbox messages as seen.'))
  })
}

/**
 * Resets all the Dockerimages in the store.
 *
 * Used to clean before changing filtering or sorting.
 *
 * @param {{exclude: object}} [{exclude={}}] data - Dockerimage ids that should not be reset
 * @return {Promise}
 */
export function resetDockerimages ({ exclude } = {}) {
  return new Promise((resolve, reject) => {
    $store.dispatch('api/resetDockerimages', { exclude })
      .then(() => resolve())
      .catch(() => reject())
  })
}

/**
 * Reads all Dockerimages from the API.
 * The store handles the data storage.
 *
 * @param {{page: number, size: number, sort: string}} data - The reading data
 * @return {Promise}
 */
export function readAllDockerimages ({ page, size, sort } = {}) {
  return new Promise((resolve, reject) => {
    const isAdmin = $store.getters['user/isAdmin'],
      username = $store.getters['user/username'],
      url = isAdmin ? '/admins/images' : `/users/${username}/dockerimages`

    return createResourceRequest()
      .get(url, { params: { page, size, sort } })
      .then(response => $store.dispatch('api/readAllDockerimages', response.data))
      .then(() => resolve())
      .catch(() => reject('Something went wrong reading all Dockerimage.'))
  })
}

/**
 * Reads one Dockerimage from the API.
 * The store handles the data storage.
 *
 * @param {string} id - The Dockerimage id
 * @return {Promise}
 */
export function readOneDockerimage (id) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .get(`/users/${username}/dockerimages/${id}`, {})
      .then(response => $store.dispatch('api/readOneDockerimage', response.data))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while reading the Dockerimage.'))
  })
}

/**
 * Updates one Dockerimage at the the API.
 * The store handles updating the data storage.
 *
 * @param {{id: string, name: string, project: string}} data - The updating data
 * @return {Promise}
 */
export function updateDockerimage (data) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .put(`/users/${username}/dockerimages/${data.id}`, {
        name: data.name,
        project: data.project,
      })
      .then(() => $store.dispatch('api/updateDockerimage', { ...data }))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while updating the Dockerimage.'))
  })
}

/**
 * Builds one Dockerimage at the the API.
 *
 * @param {string} id - The Dockerimage id
 * @param {object} data - The building data
 * @return {Promise}
 */
export function buildDockerimage (id, data) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .post(`/users/${username}/dockerimages/build/${id}`, data)
      .then(response => {
        if (response.data.fail.length > 0) {
          return reject(`Failed to build: ${response.data.fail}`)
        }

        resolve('Successfully built the Image.')
      })
      .catch(() => reject('Something went wrong while building the Dockerimage.'))
  })
}
/**
 * Reads the source files of one Dockerimage at the the API.
 *
 * @param {string} id - The Dockerimage id
 * @return {Promise}
 */
export function readDockerimageSource(id) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .get(`/users/${username}/dockerimages/build/${id}`)
      .then(response => resolve(response.data))
      .catch(() => reject('Something went wrong while getting the source files of the Dockerimage.'))
  })
}

/**
 * Deletes one Dockerimage at the the API.
 *
 * @param {string} id - The Dockerimage id
 * @return {Promise}
 */
export function deleteDockerimage (id) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    createResourceRequest()
      .delete(`/users/${username}/dockerimages/${id}`, {})
      .then(() => $store.dispatch('api/deleteDockerimage', id))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while deleting the Dockerimage.'))
  })
}

/**
 * Resets all the Jobs in the store.
 *
 * Used to clean before changing filtering or sorting.
 *
 * @param {{exclude: object}} [{exclude={}}] data - Job ids that should not be reset
 * @return {Promise}
 */
export function resetJobs ({ exclude } = {}) {
  return new Promise((resolve, reject) => {
    $store.dispatch('api/resetJobs', { exclude })
      .then(() => resolve())
      .catch(() => reject())
  })
}

/**
 * Creates one Job at the API. The store handles updating the data storage.
 *
 * @param {{imageId: string, name: string, environmentVariables: object, priority: string}} data - The creation data
 * @return {Promise}
 */
export function createJob(data) {
  return new Promise((resolve, reject) => {
    let createdJobId
    const username = $store.getters['user/username'],
      isAdmin = $store.getters['user/isAdmin'],
      options = {
        imageId: data.imageId,
        name: data.name,
        environmentVariables: data.environmentVariables,
      }

    if (isAdmin) options.priority = data.priority

    return createResourceRequest()
      .post(`/users/${username}/jobs`, options)
      .then((response) => createdJobId = response.data.id)
      .then(() => readOneJob(createdJobId))
      .then(() => resolve(createdJobId))
      .catch(() => reject('Something went wrong while creating the Job.'))
  })
}

/**
 * Reads all Jobs from the API. The store handles the data storage.
 *
 * If a specific job status is requested, read from the filtered endpoint.
 *
 * @param {{status: object, page: number, size: number, sort: string}} data - The reading data
 * @return {Promise}
 */
export function readAllJobs ({ status, page, size, sort } = { status: JobStatusFilteringTypes.ALL }) {
  return new Promise((resolve, reject) => {
    const isAdmin = $store.getters['user/isAdmin'],
      username = $store.getters['user/username'],
      url = isAdmin ? `/admins${status.endpoint}` : `/users/${username}${status.endpoint}`

    return createResourceRequest()
      .get(url, { params: { page, size, sort } })
      .then(response => $store.dispatch('api/readAllJobs', response.data))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while reading all Jobs.'))
  })
}

/**
 * Reads one Job from the API. The store handles the data storage.
 *
 * @param {number} id - The Job id
 * @return {Promise}
 */
export function readOneJob (id) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .get(`/users/${username}/jobs/${id}`, {})
      .then(response => $store.dispatch('api/readOneJob', response.data))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while reading the Job.'))
  })
}

/**
 * Updates one Job at the API. The store handles updating the data storage.
 *
 * @param {{id: number, name: string, project: string, environmentVariables: object, priority: string}} data - The updating data
 * @return {Promise}
 */
export function updateJob (data) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']
    let options = {
      name: data.name,
      project:  data.project,
      environmentVariables: data.environmentVariables,
    }

    if ($store.getters['user/isAdmin']) {
      options.priority = data.priority
    }

    return createResourceRequest()
      .put(`/users/${username}/jobs/${data.id}`, options)
      .then(() => $store.dispatch('api/updateJob', { ...data }))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while updating the Job.'))
  })
}

/**
 * Deletes one Job at the the API.
 *
 * @param {number} id - The Job id
 * @return {Promise}
 */
export function deleteJob (id) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    createResourceRequest()
      .delete(`/users/${username}/jobs/${id}`, {})
      .then(() => $store.dispatch('api/deleteJob', id))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while deleting the Job.'))
  })
}

/**
 * Starts a Job at the API. The store handles updating the data storage.
 *
 * @param {number} id - The Job id
 * @return {Promise}
 */
export function startJob(id) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .patch(`/users/${username}/jobs/${id}/start`)
      .then(() => new Promise(resolve => setTimeout(resolve, 1000)))
      .then(() => readOneJob(id))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while starting the Job.'))
  })
}

/**
 * Cancels/Aborts a Job at the API.
 * The store handles updating the data storage.
 *
 * @param {number} id - The Job id
 * @return {Promise}
 */
export function cancelJob(id) {
  return _cancelJob(id)
}

/**
 * Cancels/Aborts a Job with a reason of any user at the API.
 * The store handles updating the data storage.
 *
 * @param {{id: number, reason: string}} data - The canceling data
 * @return {Promise}
 */
export function cancelJobAdmin (data) {
  return _cancelJob(data.id, data.reason)
}

/**
 * Cancels/Aborts a Job at the API.
 * The store handles updating the data storage.
 *
 * @param {number} id - The Job id
 * @param {string} [reason=null] reason - The reason for canceling
 * @return {Promise}
 */
function _cancelJob(id, reason = '') {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    return createResourceRequest()
      .patch(`/users/${username}/jobs/${id}/cancel`, reason ? { reason } : null)
      .then(() => readOneJob(id))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while canceling the Job.'))
  })
}

/**
 * Refreshes the status of a range of Jobs from the API.
 * The store handles the data storage.
 *
 * @param {array} ids - The Job ids
 * @return {Promise}
 */
export function refreshJobStatuses (ids) {
  return new Promise((resolve, reject) => {
    const username = $store.getters['user/username']

    createResourceRequest()
      .get(`/users/${username}/jobs/statuses`, { params: { ids: ids.toString() } })
      .then(response => response.data.forEach(job => $store.dispatch('api/updateJob', { ...job })))
      .then(() => resolve())
      .catch(() => reject('Something went wrong while refreshing the Job statuses.'))
  })
}
