import $store from '@/store'
import AppConfig from '@/config'
import ClientOAuth2 from 'client-oauth2'
import * as apiService from '@/services/api.service'

const oAuth = new ClientOAuth2(AppConfig.oAuth)

/**
 * Redirects to the auth server to start the authorization.
 *
 * Part of the OAuth 2.0 authorization code flow.
 *
 * @param {{signUp: boolean}} status - Holds additional information
 */
export function authorize (status = {}) {
  const isSignUp = status.signUp ? status.signUp : false

  if (isSignUp) {
    window.location = AppConfig.urls.authServer.signUp
  } else {
    window.location = oAuth.code.getUri()
  }
}

/**
 * Requests the actual access token at the auth server with the code from the callback url.
 *
 * Part of the OAuth 2.0 authorization code flow.
 *
 * @param {string} url - The call url that holds the authorization code
 * @return {Promise}
 */
export function callback (url) {
  return new Promise((resolve, reject) => {
    oAuth.code
      .getToken(url)
      .then(user => {
        const { accessToken, expires } = user

        if (!accessToken || !expires || isAccessTokenExpired(expires))
          return reject('invalid_user')

        storeAccessToken(accessToken)

        $store
          .dispatch('user/getCurrent')
          .then(() => resolve())
          .catch(error => reject(error.message))
      })
      .catch(error => reject(error.body.error))
  })
}

/**
 * Submits the login data.
 *
 * @param {{password: string, username: string}} data - The username and password
 */
export function login (data) {
  const CSRF_TOKEN = document.cookie.match(new RegExp('XSRF-TOKEN=([^;]+)'))[1]

  submitFormData(AppConfig.urls.authServer.login, {
    ...data,
    _csrf: CSRF_TOKEN,
  })
}

/**
 * Submits the sign-up data.
 *
 * @param {{ username: string, password: string, confirmPassword: string, email: string, terms: boolean}} data - The sign up data
 */
export function signUp (data) {
  const CSRF_TOKEN = document.cookie.match(new RegExp('XSRF-TOKEN=([^;]+)'))[1]

  submitFormData(AppConfig.urls.authServer.signUp, {
    ...data,
    _csrf: CSRF_TOKEN,
  })
}

/**
 * Requests a logout at the auth server.
 *
 * @return {Promise}
 */
export function logout () {
  return new Promise((resolve, reject) => {
    apiService
      .revokeToken()
      .then(() => resolve())
      .catch(error => reject(error))
      .finally(() => {
        $store
          .dispatch('user/removeCurrent')
          .finally(removeAccessToken)
    })
  })
}

/**
 * Calculates if the current access token is expired.
 *
 * @param {number} expires - The expiry date
 * @return {boolean}
 */
export function isAccessTokenExpired (expires) {
  return expires <= Math.floor(new Date().getTime() / 1000)
}

/**
 * Gets the current access token.
 *
 * @return {string}
 */
export function getAccessToken () {
  return localStorage.getItem('accessToken')
}

/**
 * Remove access token from a semi-persistent store.
 */
export function removeAccessToken () {
  localStorage.setItem('accessToken', '')
}

/**
 * Store access token in a semi-persistent store.
 *
 * @param {string} accessToken - The access token
 */
function storeAccessToken (accessToken) {
  localStorage.setItem('accessToken', accessToken)
}

/**
 * Submits a form to a specific url. This will change the window location.
 *
 * @param {string} url - The path to send the data to
 * @param {object} params - The parameters to add to the url
 * @param {string} [method=POST] - The method to use on the form
 */
function submitFormData (url, params, method = 'POST') {
  const form = document.createElement('form')
  form.method = method
  form.action = url

  for (const key in params) {
    if (Object.prototype.hasOwnProperty.call(params, key)) {
      const field = document.createElement('input')
      field.type = 'hidden'
      field.name = key
      field.value = params[key]

      form.appendChild(field)
    }
  }

  document.body.appendChild(form)
  form.submit()
}
