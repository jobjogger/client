/**
 * This is a global config class.
 */

import Environment from '@/../environment'

const AppConfig = {}

AppConfig.hosts = {
  client: Environment.value('clientHost'),
  authServer: Environment.value('authServerHost'),
  resourceServer: Environment.value('resourceServerHost'),
  reportingService: Environment.value('reportingServiceHost'),
  dashboard: Environment.value('dashboardHost'),
}

AppConfig.endpoints = {
  authServer: {
    login: '/auth/login',
    signUp: '/auth/registration',
    checkToken: '/auth/oauth/check_token',
    revokeToken: '/auth/oauth/revoke_token',
  },
}

AppConfig.urls = {
  authServer: {
    login: AppConfig.hosts.authServer + AppConfig.endpoints.authServer.login,
    signUp: AppConfig.hosts.authServer + AppConfig.endpoints.authServer.signUp,
  },
}

AppConfig.oAuth = {
  clientId: Environment.value('oAuthClientId'),
  clientSecret: Environment.value('oAuthClientSecret'),
  accessTokenUri: `${AppConfig.hosts.authServer}/auth/oauth/token`,
  authorizationUri: `${AppConfig.hosts.authServer}/auth/oauth/authorize`,
  redirectUri: `${AppConfig.hosts.client}/auth/callback`,
}

export default AppConfig
