describe('Auth Service', () => {
  describe('authorize()', () => {
  })

  describe('callback()', () => {
  })

  describe('logout()', () => {
    it('should be resolved if the user was logged out', (done) => {
      done()
    })

    it('should be rejected if there is no access token', (done) => {
      done()
    })
  })

  describe('isAccessTokenExpired()', () => {
    it('should be true if the accessToken is expired', () => {
    })

    it('should be false if the accessToken is not expired', () => {
    })
  })

  describe('getAccessToken()', () => {
    it('should return the accessToken if one set', () => {
    })

    it('should return nothing if none set', () => {
    })
  })
})
