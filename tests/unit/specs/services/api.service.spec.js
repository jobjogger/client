import * as apiService from '@/services/api.service'

describe('API Service', () => {
  describe('checkToken()', () => {
    it('should be rejected if no access token exists', (done) => {
      apiService.checkToken().catch(() => done())
    })

    it('should be resolved if the token was checked', (done) => {
      done()
    })
  })

  describe('createResourceRequest()', () => {
  })

  describe('readAllDockerimages()', () => {
    it('should read all Dockerimages from the server and store them', () => {
    })
  })

  describe('readOneDockerimage()', () => {
    it('should read one Dockerimages from the server and store it', () => {
    })
  })

  describe('updateDockerimage()', () => {
    it('should update one Dockerimages at the server and in the store', () => {
    })
  })

  describe('deleteDockerimage()', () => {
    it('should delete one Dockerimages at the server and from the store', () => {
    })
  })
})
