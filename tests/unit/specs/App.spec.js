import App from '@/App'
import ElementUI from 'element-ui'
import Vuex from 'vuex'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)
localVue.use(Vuex)

describe('App.vue', () => {
  let store = new Vuex.Store({
    modules: {
      app: {
        namespaced: true,
        getters: {
          watchedJobs: () => [],
        },
      },
    },
  })

  it('should render the main app container', () => {
    const wrapper = mount(App, {
      localVue,
      store,
      stubs: ['app-header', 'router-view'],
      mocks: {
        $route: {},
        $userIsLoggedIn: false,
      },
    })

    expect(wrapper.contains('div#app')).toBe(true)
  })

  it('should render #menu-container if logged in', () => {
    const wrapper = mount(App, {
      localVue,
      store,
      stubs: ['app-header', 'router-view'],
      mocks: {
        $route: {},
        $userIsLoggedIn: true,
      },
    })

    expect(wrapper.contains('#menu-container')).toBe(true)
  })

  it('should not render #menu-container if not logged in', () => {
    const wrapper = mount(App, {
      localVue,
      store,
      stubs: ['app-header', 'router-view'],
      mocks: {
        $route: {},
        $userIsLoggedIn: false,
      },
    })

    expect(wrapper.contains('#menu-container')).toBe(false)
  })

  it('should render #main-container', () => {
    const wrapper = mount(App, {
      localVue,
      store,
      stubs: ['app-header', 'router-view'],
      mocks: {
        $route: {},
        $userIsLoggedIn: false,
      },
    })

    expect(wrapper.contains('#main-container')).toBe(true)
  })

  it('should render #side-container if route has side component', () => {
    const wrapper = mount(App, {
      localVue,
      store,
      stubs: ['app-header', 'router-view'],
      mocks: {
        $route: { matched: [{ components: { side: 1 } }] },
        $userIsLoggedIn: true,
      },
    })

    expect(wrapper.contains('#side-container')).toBe(true)
  })

  it('should not render #side-container if route has no side component', () => {
    const wrapper = mount(App, {
      localVue,
      store,
      stubs: ['app-header', 'router-view'],
      mocks: {
        $route: {},
        $userIsLoggedIn: true,
      },
    })

    expect(wrapper.contains('#side-container.collapsed')).toBe(true)
  })
})
