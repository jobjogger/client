import Inbox from '@/views/Inbox'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('Inbox.vue', () => {
  it('should render the headline', () => {
    const wrapper = mount(Inbox, { localVue }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
