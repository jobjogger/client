import Dashboard from '@/views/Dashboard'
import ElementUI from 'element-ui'
import Vuex from 'vuex'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)
localVue.use(Vuex)

describe('Dashboard.vue', () => {
  let store = new Vuex.Store({
    modules: {
      app: {
        namespaced: true,
        actions: {
          watchJobs: () => [],
        },
      },
      api: {
        namespaced: true,
        getters: {
          allJobs: () => [],
        },
      },
    },
  })

  it('should render the headline', () => {
    const wrapper = mount(Dashboard, {
      localVue,
      store,
      stubs: ['router-link'],
    })
    const headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
