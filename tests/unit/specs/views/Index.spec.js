import Index from '@/views/Index'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('Index.vue', () => {
  it('should render the headline', () => {
    const wrapper = mount(Index, { localVue }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
