import Login from '@/views/auth/Login'
import ElementUI from 'element-ui'
import { createLocalVue, shallowMount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('Login.vue', () => {
  it('should render the headline', () => {
    const wrapper = shallowMount(Login, { localVue }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
