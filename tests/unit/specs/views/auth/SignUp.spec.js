import SignUp from '@/views/auth/SignUp'
import ElementUI from 'element-ui'
import { createLocalVue, shallowMount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('SignUp.vue', () => {
  it('should render the headline', () => {
    const wrapper = shallowMount(SignUp, { localVue }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
