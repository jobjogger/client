import FourOFour from '@/views/errors/404'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('404.vue', () => {
  it('should render the headline', () => {
    const wrapper = mount(FourOFour, { localVue }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})

