import ImagesOverview from '@/views/images/ImagesOverview'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('ImagesOverview.vue', () => {
  let store = new Vuex.Store({
    modules: {
      app: {
        namespaced: true,
        getters: {
          selectedDockerimagesOverviewSorting: () => {
            return {}
          },
        },
        actions: {
          watchJobs: () => [],
        },
      },
      api: {
        namespaced: true,
        getters: {
          allDockerimages: () => [],
        },
      },
    },
  })

  it('should render the headline', () => {
    const wrapper = mount(ImagesOverview, {
        localVue,
        store,
      }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
