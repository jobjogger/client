import ImageDetails from '@/views/images/ImageDetails'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('ImageDetails.vue', () => {
  let store = new Vuex.Store({
    modules: {
      api: {
        namespaced: true,
        getters: {
          dockerimageWithId: () => (id) => {
            return { id: id, meta: {} }
          },
        },
      },
    },
  })

  it('should render the headline', () => {
    const wrapper = mount(ImageDetails, {
        localVue,
        store,
        mocks: {
          $route: { params: { id: 1 } },
        },
      }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
