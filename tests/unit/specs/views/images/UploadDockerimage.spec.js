import UploadDockerimage from '@/views/images/UploadDockerimage'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('UploadDockerimage.vue', () => {
  it('should render the headline', () => {
    const wrapper = mount(UploadDockerimage, { localVue }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
