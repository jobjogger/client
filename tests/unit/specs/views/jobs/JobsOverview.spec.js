import JobsOverview from '@/views/jobs/JobsOverview'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('JobsOverview.vue', () => {
  let store = new Vuex.Store({
    modules: {
      app: {
        namespaced: true,
        getters: {
          selectedJobsOverviewFiltering: () => {
            return {}
          },
          selectedJobsOverviewSorting: () => {
            return {}
          },
        },
        actions: {
          watchJobs: () => [],
        },
      },
      api: {
        namespaced: true,
        getters: {
          allJobs: () => [],
        },
      },
    },
  })

  it('should render the headline', () => {
    const wrapper = mount(JobsOverview, {
        localVue,
        store,
        mocks: {
          $route: {},
        },
      }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
