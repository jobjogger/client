import JobDetails from '@/views/jobs/JobDetails'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('JobDetails.vue', () => {
  let store = new Vuex.Store({
    modules: {
      api: {
        namespaced: true,
        getters: {
          jobWithId: () => (id) => {
            return { id: id, priority: '', status: '', meta: {} }
          },
        },
      },
    },
  })

  it('should render the headline', () => {
    const wrapper = mount(JobDetails, {
        localVue,
        store,
        stubs: ['job-details-logs'],
        mocks: {
          $route: { params: { id: 1 } },
        },
      }),
      headline = wrapper.find('h1')

    expect(headline.exists()).toBe(true)
  })
})
