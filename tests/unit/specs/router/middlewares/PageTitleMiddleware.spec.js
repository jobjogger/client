import PageTitleMiddleware from '@/router/middlewares/PageTitleMiddleware'

describe('PageTitleMiddleware', () => {
  it('should set the correct page title and proceed', (done) => {
    const pageTitle = 'Test'

    PageTitleMiddleware({ matched: [{ meta: { title: pageTitle } }] }, {}, (args) => {
      expect(global.document.title).toContain(pageTitle)
      expect(args).toBeUndefined()
      done()
    })
  })
})
