import AuthMiddleware from '@/router/middlewares/AuthMiddleware'

// Fix "Cannot read property 'getters' of undefined at CurrentUserStateMiddleware"
describe('AuthMiddleware', () => {
  it('should redirect to dashboard if user is logged in and route requires noAuth', (done) => {
    done()
  })

  it('should redirect to login if user is not logged in and route requires auth', (done) => {
    done()
  })

  it('should proceed if route does not require auth', (done) => {
    AuthMiddleware({ matched: [] }, {}, (args) => {
      expect(args).toBeUndefined()
      done()
    })
  })
})
