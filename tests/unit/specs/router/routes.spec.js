import routes from '@/router/routes'

describe('Routes', () => {
  describe('/dashboard', () => {
    const route = routes.find(r => r.path === '/dashboard')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/images/overview', () => {
    const route = routes.find(r => r.path === '/images/overview')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/images/details/:id', () => {
    const route = routes.find(r => r.path === '/images/details/:id')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/images/upload', () => {
    const route = routes.find(r => r.path === '/images/upload')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/images/edit/:id', () => {
    const route = routes.find(r => r.path === '/images/edit/:id')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/images/build', () => {
    const route = routes.find(r => r.path === '/images/build')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/images/build/template/:key', () => {
    const route = routes.find(r => r.path === '/images/build/template/:key')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/jobs/overview', () => {
    const route = routes.find(r => r.path === '/jobs/overview')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/jobs/details/:id', () => {
    const route = routes.find(r => r.path === '/jobs/details/:id')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/inbox', () => {
    const route = routes.find(r => r.path === '/inbox')

    it('should require auth', () => {
      expect(route.meta.requiresAuth).toBe(true)
    })
  })

  describe('/404', () => {
    const route = routes.find(r => r.path === '/404')

    it('should require neither auth nor noAuth', () => {
      expect(route.meta.requiresAuth).toBeUndefined()
    })
  })

  describe('Any undefined', () => {
    const route = routes.find(r => r.path === '*')

    it('should redirect to error page', () => {
      expect(route.redirect).toBe('404')
    })
  })
})
