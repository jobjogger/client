import AppMenu from '@/components/layout/AppMenu'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('AppMenu.vue', () => {
  it('should render the menu element', () => {
    const wrapper = mount(AppMenu, {
      localVue,
      stubs: ['el-menu-item', 'el-submenu'],
      mocks: {
        $route: {},
      },
    })

    expect(wrapper.contains('.el-menu')).toBe(true)
  })
})
