import AppHeader from '@/components/layout/AppHeader'
import ElementUI from 'element-ui'
import { createLocalVue, mount } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(ElementUI)

describe('AppHeader.vue', () => {
  it('should render the header element', () => {
    const wrapper = mount(AppHeader, {
      localVue,
      stubs: [],
      mocks: {
        $route: {},
        $userIsLoggedIn: false,
        $isLoginRoute: false,
        $isSignUpRoute: false,
      },
    })

    expect(wrapper.contains('.el-header')).toBe(true)
  })

  it('should render the username if logged in', () => {
    const wrapper = mount(AppHeader, {
      localVue,
      stubs: ['router-link'],
      mocks: {
        $route: {},
        $userIsLoggedIn: true,
        $currentUser: { username: 'test_user' },
        $isLoginRoute: false,
        $isSignUpRoute: false,
      },
    })

    expect(wrapper.find({ ref: 'username' }).text()).toBe('test_user')
  })

  it('should render no auth buttons if logged in', () => {
    const wrapper = mount(AppHeader, {
      localVue,
      stubs: ['router-link'],
      mocks: {
        $route: {},
        $userIsLoggedIn: true,
        $currentUser: { username: 'test_user' },
        $isLoginRoute: false,
        $isSignUpRoute: false,
      },
    })

    expect(wrapper.find({ ref: 'auth-buttons' }).exists()).toBeFalsy()
  })

  it('should render auth buttons if not logged in', () => {
    const wrapper = mount(AppHeader, {
      localVue,
      stubs: ['router-link'],
      mocks: {
        $route: {},
        $userIsLoggedIn: false,
        $isLoginRoute: false,
        $isSignUpRoute: false,
      },
    })

    expect(wrapper.find({ ref: 'auth-buttons' }).exists()).toBe(true)
  })

  describe('Logout button', () => {
    it('should be rendered if logged in', () => {
      const wrapper = mount(AppHeader, {
        localVue,
        stubs: ['router-link'],
        mocks: {
          $route: {},
          $userIsLoggedIn: true,
          $currentUser: { username: 'test_user' },
          $isLoginRoute: false,
          $isSignUpRoute: false,
        },
      })

      expect(wrapper.find({ ref: 'logout-button' }).exists()).toBe(true)
    })
  })

  describe('Login button', () => {
    it('should be rendered if route is sign-up', () => {
      const wrapper = mount(AppHeader, {
        localVue,
        stubs: ['router-link'],
        mocks: {
          $route: {},
          $userIsLoggedIn: false,
          $isLoginRoute: false,
          $isSignUpRoute: true,
        },
      })

      expect(wrapper.find({ ref: 'login-button' }).exists()).toBe(true)
    })
  })

  describe('Sign-Up button', () => {
    it('should be rendered if route is login', () => {
      const wrapper = mount(AppHeader, {
        localVue,
        stubs: ['router-link'],
        mocks: {
          $route: {},
          $userIsLoggedIn: false,
          $isLoginRoute: true,
          $isSignUpRoute: false,
        },
      })

      expect(wrapper.find({ ref: 'sign-up-button' }).exists()).toBe(true)
    })
  })
})
