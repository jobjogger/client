import faker from 'faker'

Cypress.Commands.add('authenticate', () => {
  let username = faker.internet.userName().toLowerCase().replace(/_/g, '-').substring(0, 32),
    password = faker.internet.password(),
    email = faker.internet.email()

  Cypress.log({ name: 'Sign-Up' })

  cy.clearCookies()
  cy.clearLocalStorage()

  cy.visit(Cypress.env('clientUrl'), {
    onBeforeLoad: (win) => {
      win.sessionStorage.clear()
    },
  })

  cy.get('button').contains('Sign Up').click()
  cy.url().should('be', Cypress.env('signUpUrl'))

  cy.shadowGet('job-jogger-sign-up')
    .shadowFind('input[name=username]')
    .shadowFirst()
    .shadowType(username)
    .shadowTrigger('input')

  cy.shadowGet('job-jogger-sign-up')
    .shadowFind('input[name=password]')
    .shadowFirst()
    .shadowType(password)
    .shadowTrigger('input')

  cy.shadowGet('job-jogger-sign-up')
    .shadowFind('input[name=confirm-password]')
    .shadowFirst()
    .shadowType(password)
    .shadowTrigger('input')

  cy.shadowGet('job-jogger-sign-up')
    .shadowFind('input[name=email]')
    .shadowFirst()
    .shadowType(email)
    .shadowTrigger('input')

  cy.shadowGet('job-jogger-sign-up')
    .shadowFind('input[name=terms]')
    .shadowFirst()
    .shadowCheck()
    .shadowTrigger('change')

  cy.shadowGet('job-jogger-sign-up')
    .shadowFind('#sign-up-button')
    .shadowFirst()
    .shadowClick()

  Cypress.log({ name: 'Login' })

  cy.clearCookies()
  cy.clearLocalStorage()

  cy.visit(Cypress.env('clientUrl'), {
    onBeforeLoad: (win) => {
      win.sessionStorage.clear()
    },
  })

  cy.get('button').contains('Login').click()

  cy.url().should('be', Cypress.env('loginUrl'))

  cy.shadowGet('job-jogger-login')
    .shadowFind('input[name=username]')
    .shadowFirst()
    .shadowType(username)
    .shadowTrigger('input')

  cy.shadowGet('job-jogger-login')
    .shadowFind('input[name=password]')
    .shadowFirst()
    .shadowType(password)
    .shadowTrigger('input')

  cy.shadowGet('job-jogger-login')
    .shadowFind('#login-button')
    .shadowFirst()
    .shadowClick()

  cy.url()
    .should('include', 'code=')
    .should('include', '/auth/callback')
    .should('include', Cypress.env('clientUrl'))

  cy.url()
    .should('include', '/dashboard')
    .should('include', Cypress.env('clientUrl'))

  Cypress.log({ name: 'Access Token validation' })

  let accessToken

  cy.window().should((window) => {
    accessToken = window.localStorage.getItem('accessToken')
    expect(accessToken).to.be.a('string').and.not.be.empty
  }).then(() => {
    const options = {
      method: 'POST',
      url: Cypress.env('checkTokenUrl'),
      qs: { token: accessToken },
      failOnStatusCode: false,
    }

    cy.request(options).should((response) => {
      expect(response.status).eq(200)
      expect(response.body['user_name']).eq(username)
    }).then(() => {
      return cy.wrap({ username, password, email, accessToken })
    })
  })
})

Cypress.Commands.add('revokeAuthentication', (accessToken) => {
  const options = {
    method: 'DELETE',
    url: Cypress.env('revokeTokenUrl'),
    auth: { 'bearer': accessToken },
  }

  cy.request(options).should((response) => {
    expect(response.status).eq(204)
  }).then(() => {
    const options = {
      method: 'POST',
      url: Cypress.env('checkTokenUrl'),
      qs: { token: accessToken },
      failOnStatusCode: false,
    }

    cy.request(options).should((response) => {
      expect(response.status).eq(400)
    })
  })
})

Cypress.Commands.add('testUnauthenticated', (method, url, accessToken = '', status = 401) => {
  cy.request({ method, url, auth: { 'bearer': accessToken }, body: {}, failOnStatusCode: false })
    .then((response) => {
      expect(response.status).eq(status)
    })
})
