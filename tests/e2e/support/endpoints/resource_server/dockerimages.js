const resourceServerUrl = Cypress.env('resourceServerUrl')

Cypress.Commands.add('createOneDockerimage', (username, accessToken, { fileName, fileType }) => {
  return cy.uploadFile('POST', `${resourceServerUrl}/users/${username}/dockerimages`, accessToken, {
    fileName,
    fileType,
  })
    .then((response) => {
      expect(response.status).to.eq(200)
      expect(response.body.fail).to.be.a('array').and.be.empty
      expect(response.body.success).to.be.a('array').and.to.have.lengthOf(1)

      return response.body.success[0]
    })
})

Cypress.Commands.add('createMultipleDockerimages', (username, accessToken, { fileName, fileType, imageCount }) => {
  return cy.uploadFile('POST', `${resourceServerUrl}/users/${username}/dockerimages`, accessToken, {
    fileName,
    fileType,
  })
    .then((response) => {
      expect(response.status).to.eq(200)
      expect(response.body.fail).to.be.a('array').and.be.empty
      expect(response.body.success).to.be.a('array').and.to.have.lengthOf(imageCount)

      return response.body.success
    })
})

Cypress.Commands.add('readOneDockerimage', (username, accessToken, imageId, failOnStatusCode = true) => {
  return cy.request({
    method: 'GET',
    url: `${resourceServerUrl}/users/${username}/dockerimages/${imageId}`,
    auth: { 'bearer': accessToken },
    failOnStatusCode,
  })
})

Cypress.Commands.add('readAndCheckOneDockerimage', (username, accessToken, imageId) => {
  return cy.readOneDockerimage(username, accessToken, imageId)
    .then((response) => {
      expect(response.status).to.eq(200)
      expect(response.body).to.be.a('object').and.not.be.empty

      return response.body
    })
})

Cypress.Commands.add('createAndReadOneDockerimage', (username, accessToken) => {
  return cy.createOneDockerimage(username, accessToken, {
    fileName: 'resource_server/single-image.tar',
    fileType: 'application/x-tar',
  }).then((image) => cy.readAndCheckOneDockerimage(username, accessToken, image.image))
})

Cypress.Commands.add('createAndReadMultipleDockerimage', (username, accessToken) => {
  return cy.createMultipleDockerimages(username, accessToken, {
    fileName: 'resource_server/double-image.tar',
    fileType: 'application/x-tar',
    imageCount: 2,
  }).then((images) => Promise.all(images.map(image => cy.readAndCheckOneDockerimage(username, accessToken, image.image))))
})
