import './dockerimages'

const resourceServerUrl = Cypress.env('resourceServerUrl')

Cypress.Commands.add('createOneJob', (username, accessToken) => {
  const jobName = 'TEST'
  let createdImage

  return cy.createAndReadOneDockerimage(username, accessToken)
    .then(image => createdImage = image)
    .then(() => cy.request({
      method: 'POST',
      url: `${resourceServerUrl}/users/${username}/jobs`,
      auth: { 'bearer': accessToken },
      body: { 'imageId': createdImage.id, 'name': jobName },
    }))
    .then((response) => {
      expect(response.status).to.eq(200)
      expect(response.body).to.be.a('object').and.not.be.empty
      expect(response.body.status).to.eq('NONE')
      expect(response.body.name).to.eq(jobName)

      return response.body
    })
})

Cypress.Commands.add('readOneJob', (username, accessToken, jobId, failOnStatusCode = true) => {
  return cy.request({
    method: 'GET',
    url: `${resourceServerUrl}/users/${username}/jobs/${jobId}`,
    auth: { 'bearer': accessToken },
    failOnStatusCode,
  })
})

Cypress.Commands.add('readAndCheckOneJob', (username, accessToken, jobId) => {
  return cy.readOneJob(username, accessToken, jobId)
    .then((response) => {
      expect(response.status).to.eq(200)
      expect(response.body).to.be.a('object').and.not.be.empty

      return response.body
    })
})

Cypress.Commands.add('createAndReadOneJob', (username, accessToken) => {
  let createdJob

  return cy.createOneJob(username, accessToken)
    .then(job => createdJob = job)
    .then(() => cy.readOneJob(username, accessToken, createdJob.id))
    .then(response => {
      expect(response.status).to.eq(200)
      expect(response.body).to.be.a('object').and.not.be.empty
      expect(response.body.id).to.eq(createdJob.id)

      return response.body
    })
})
