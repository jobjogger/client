Cypress.Commands.add('uploadFile', (method, url, accessToken, { fileName, fileType, fileAttribute = 'file' }) => {
  cy.fixture(fileName, 'base64')
    .then(fixture => Cypress.Blob.base64StringToBlob(fixture, fileType))
    .then(blob => {
      return new Cypress.Promise((resolve, reject) => {
        const data = new FormData()
        data.set(fileAttribute, blob)

        const xhr = new XMLHttpRequest()
        xhr.withCredentials = true
        xhr.timeout = 25000
        xhr.open(method, url)
        xhr.setRequestHeader('Authorization', `Bearer ${accessToken}`)
        xhr.onload = () => {
          resolve({
            status: xhr.status,
            body: JSON.parse(xhr.response),
          })
        }
        xhr.onerror = () => {
          reject({
            status: xhr.status,
            body: JSON.parse(xhr.response),
          })
        }
        xhr.send(data)
      })
    })
})
