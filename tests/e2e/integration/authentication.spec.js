describe('Authentication', () => {
  context('Sign-Up and Login to obtain a access token', () => {
    it('redirects to login when not logged in', () => {
      cy.visit(Cypress.env('clientUrl') + '/dashboard')
      cy.url().should('be', Cypress.env('loginUrl'))
    })

    it('authenticates a new user', () => {
      cy.authenticate().then(user => {
        cy.revokeAuthentication(user.accessToken)
      })
    })
  })
})
